<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('M_inicio');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }

    public function index() {
        $data['main'] = ['inicio'];
        $data['types'] = $this->M_inicio->get_types();
        $this->load->view('templates/template', $data);
    }

    public function get_countries() {
        $result = $this->M_inicio->_get_countries($this->input->post());
        echo json_encode(($result != FALSE) ? ['success' => TRUE, 'suggestions' => $result] : ['success' => FALSE, 'data' => []]);
    }

    public function save_pyme() {
        if ($this->input->post()) {
            if ($this->form_validation->run() === FALSE)
                $this->index();
            else {
                $result = $this->M_inicio->saving_pyme($this->input->post());
                ($result == FALSE) ? $this->session->set_flashdata('msg', 1) : $this->session->set_flashdata('msg', 67);
                redirect(base_url());
            }
        } else
            redirect(base_url());
    }

    public function get_pymes() {
        $data = $this->input->post();
        $result = $this->M_inicio->_get_pymes($this->input->post());
        echo json_encode(($result != FALSE) ? $result : $result);
    }

    public function drop_pyme() {
        $result = $this->M_inicio->_drop_pyme($this->input->post());
        echo json_encode(($result != FALSE) ? ['success' => TRUE, 'data' => 1] : ['success' => FALSE, 'data' => 67]);
    }

    public function edit() {
        if ($this->input->is_ajax_request()) {
            if ($this->form_validation->run() === FALSE)
                echo json_encode(['success' => FALSE, 'data' => [
                        'name_emp_msg' => form_error('name_emp', '<div style="font-size: 10px" class="text-danger">', '</div>'),
                        'type_emp_msg' => form_error('type_emp', '<div style="font-size: 10px" class="text-danger">', '</div>'),
                        'country_msg' => form_error('country', '<div style="font-size: 10px" class="text-danger">', '</div>'),
                        'id_msg' => form_error('id', '<div style="font-size: 10px" class="text-danger">', '</div>'),
                        'estado_msg' => form_error('estado', '<div style="font-size: 10px" class="text-danger">', '</div>'),
                        'city_msg' => form_error('city', '<div style="font-size: 10px" class="text-danger">', '</div>'),
                        'des_emp_msg' => form_error('des_emp', '<div style="font-size: 10px" class="text-danger">', '</div>'),
                        'user_msg' => form_error('user', '<div style="font-size: 10px" class="text-danger">', '</div>')
                ]]);
            else {
                $result = $this->M_inicio->saving_pyme($this->input->post());
                echo json_encode(($result != FALSE) ? ['success' => TRUE, 'data' => 1, 'clear' => [
                                'name_emp_msg' => '', 'type_emp_msg' => '', 'country_msg' => '', 'id_msg' => '',
                                'estado_msg' => '', 'city_msg' => '', 'des_emp_msg' => '', 'user_msg' => '']] : ['success' => FALSE, 'data' => 67]);
            }
        } else
            redirect(base_url());
    }

}
