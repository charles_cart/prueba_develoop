<?php

class Migration_Add_table_business extends CI_Migration {

    public function up() {
        $this->dbforge->add_field([
            'id' => ['type' => 'INT', 'auto_increment' => TRUE],
            'type_id' => ['type' => 'INT', 'null' => FALSE],
            'country_id' => ['type' => 'INT', 'null' => FALSE],
            'name_emp' => ['type' => 'VARCHAR', 'constraint' => '250', 'null' => FALSE],
            'estado' => ['type' => 'VARCHAR', 'constraint' => '100', 'null' => FALSE],
            'city' => ['type' => 'VARCHAR', 'constraint' => '100', 'null' => FALSE],
            'user' => ['type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE],
            'pass' => ['type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE],
            'description' => ['type' => 'VARCHAR', 'constraint' => '1000', 'null' => FALSE]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('business');
        echo '~> Add table business<br>';
    }

    public function down() {
        $this->dbforge->drop_table('business', TRUE);
        echo '~> Delete table business<br>';
    }

}
