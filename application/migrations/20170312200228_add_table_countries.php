<?php

class Migration_Add_table_countries extends CI_Migration {

    public function up() {
        $this->dbforge->add_field([
            'id' => ['type' => 'INT', 'auto_increment' => TRUE],
            'name' => ['type' => 'VARCHAR', 'constraint' => '150', 'null' => FALSE],
            'siglas' => ['type' => 'VARCHAR', 'constraint' => '50', 'null' => TRUE]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('countries');
        echo '~> Add table countries<br>';
    }

    public function down() {
        $this->dbforge->drop_table('countries', TRUE);
        echo '~> Delete table countries<br>';
    }

}
