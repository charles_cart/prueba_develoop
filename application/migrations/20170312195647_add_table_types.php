<?php

class Migration_Add_table_types extends CI_Migration {

    public function up() {
        $this->dbforge->add_field([
            'id' => ['type' => 'INT', 'auto_increment' => TRUE],
            'name' => ['type' => 'VARCHAR', 'constraint' => '200', 'null' => FALSE],
            'siglas' => ['type' => 'VARCHAR', 'constraint' => '50', 'null' => FALSE],
            'description' => ['type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('types');
        echo '~> Add table types<br>';
    }

    public function down() {
        $this->dbforge->drop_table('types', TRUE);
        echo '~> Delete table types<br>';
    }

}
