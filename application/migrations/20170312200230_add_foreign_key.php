<?php

class Migration_Add_foreign_key extends CI_Migration {

    public function up() {
        // Table business
        $this->db->query('ALTER TABLE `business` ADD CONSTRAINT `fk_business_types_1` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->db->query('ALTER TABLE `business` ADD CONSTRAINT `fk_business_countries_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
    
        echo '~> Add foreign key business to types<br>';
        echo '~> Add foreign key business to countries<br>';
    }

    public function down() {
        // Table business
        $this->db->query('ALTER TABLE `business` DROP FOREIGN KEY `fk_business_types_1`;');
        $this->db->query('ALTER TABLE `business` DROP FOREIGN KEY `fk_business_countries_1`;');
        
        echo '~> Delete foreign key business to types<br>';
        echo '~> Delete foreign key business to countries<br>';
    }

}
