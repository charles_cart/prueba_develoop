<?php

class Migration_Add_view_pymes extends CI_Migration {

    public function up() {
        $this->db->query("CREATE OR REPLACE VIEW `v_pymes` AS SELECT b.*, c.name 'country', t.name 'type' FROM business b JOIN countries c ON b.country_id = c.id JOIN types t ON b.type_id = t.id;");

        echo '~> Add view v_pymes<br>';
    }

    public function down() {
        $this->db->query("DROP VIEW `v_pymes`;");

        echo '~> Delete view v_pymes<br>';
    }

}
