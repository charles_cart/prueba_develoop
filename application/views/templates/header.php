<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Charles</title>

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!--sweet alert 2-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sweetalert2.min.css">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <!--css para el autocomplete-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles.css"/>
        <!--Datatable-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/datatables.min.css"/>

        
        <?php require_once 'application/views/common/v_datos.php'; ?>
    </head>
    <body style="background-image: url(<?php echo base_url();?>assets/img/fondo.png);">
        <nav style="background-color: #fff; border-color: #ddd" class="navbar navbar-default visible-xs visible-sm">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!--<a class="navbar-brand" href="#">Project name</a>-->
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li><a href="#"><b>INICIO</b></a></li>
                        <li class="active"><a href="#"><b>EMPRESAS</b></a></li>
                        <li><a href="#"><b>SERVICIOS</b></a></li>
                        <li><a href="#"><b>VALORA UN SERVICIO</b></a></li>
                        <li class="hidden-sm"><a href="#"><b>CONTACTO</b></a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right teal">
                        <li><a style="color: #fff !important" href="#"><b>Registro</b></a></li>
                        <li class="text-muted"> </li>
                        <li><a style="color: #fff !important" href="#"><b>Iniciar Sesi&oacute;n</b></a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
        <div class="container-fluid">
            <header>
                <nav style="margin-top:50px; background-color: #fff; border-color: #ddd" class="navbar navbar-default navbar-static-top hidden-xs hidden-sm">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <!--<a class="navbar-brand" href="#">Project name</a>-->
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li><a href="#"><b>INICIO</b></a></li>
                                <li class="active"><a href="#"><b>EMPRESAS</b></a></li>
                                <li><a href="#"><b>SERVICIOS</b></a></li>
                                <li><a href="#"><b>VALORA UN SERVICIO</b></a></li>
                                <li><a href="#"><b>CONTACTO</b></a></li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right teal">
                                <li><a style="color: #fff !important" href="#"><b>Registro</b></a></li>
                                <li class="text-muted"> </li>
                                <li><a style="color: #fff !important" href="#"><b>Iniciar Sesi&oacute;n</b></a></li>
                            </ul>
                        </div><!--/.nav-collapse -->
                    </div>
                </nav>
            </header>
            <main>