            </main>
            <footer>
                <div class="row">
                <div class="col-xs-12" style="margin-top: 2%">
                    <div style="min-height: 10px;" class="degradado"></div>
                </div>
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-xs-12 col-md-3"></div>
                            
                            <div class="col-xs-12 col-sm-6 col-md-2 text-justify">
                                <h5><b>Empresas</b></h5>
                                <div class="col-xs-12" style="min-height: 10px"></div>
                                <h5 class="text-muted">Consequat</h5>
                                <h5 class="text-muted">Nunc tempor</h5>
                                <h5 class="text-muted">Ante quis libero</h5>
                                <h5 class="text-muted">Fermentum</h5>
                            </div>
                            
                            <div class="col-xs-12 col-sm-6 col-md-2 text-justify">
                                <h5><b>Servicios</b></h5>
                                <div class="col-xs-12" style="min-height: 10px"></div>
                                <h5 class="text-muted">Sed congue suscipit</h5>
                                <h5 class="text-muted">Consequat</h5>
                                <h5 class="text-muted">Nunc tempor</h5>
                                <h5 class="text-muted">Ante quis libero</h5>
                                <h5 class="text-muted">Fermentum</h5>
                            </div>
                            
                            <div class="col-xs-12 col-sm-6 col-md-2 text-justify">
                                <h5><b>Valora un Servicio</b></h5>
                                <div class="col-xs-12" style="min-height: 10px"></div>
                                <h5 class="text-muted">Sed congue suscipit</h5>
                                <h5 class="text-muted">Consequat</h5>
                                <h5 class="text-muted">Nunc tempor</h5>
                                <h5 class="text-muted">Ante quis libero</h5>
                                <h5 class="text-muted">Fermentum</h5>
                                <h5 class="text-muted">Rhoncus dolor mattis</h5>
                                <h5 class="text-muted">Nullam pellentesque</h5>
                                <h5 class="text-muted">interdum ipsum ac</h5>
                            </div>
                            
                            <div class="col-xs-12 col-sm-6 col-md-2 text-justify">
                                <h5><b>Contacto</b></h5>
                                <div class="col-xs-12" style="min-height: 10px"></div>
                                <h5 class="text-muted">Ante quis libero</h5>
                                <h5 class="text-muted">Fermentum</h5>
                                <h5 class="text-muted">Rhoncus dolor mattis</h5>
                                <h5 class="text-muted">Nullam pellentesque</h5>
                                <h5 class="text-muted">interdum ipsum ac</h5>
                            </div>
                            
                            <div style="margin-top: 3%" class="col-xs-12 col-md-9 pull-right">
                                <p><b>Opiniones y consejos sobre servicios sanitarios, funerarios y mucho más.</b></p>
                                <p class="text-muted text-justify">© 2017 Todos los derechos reservados. <ins>Condiciones de uso,</ins> <ins>Política de privacidad</ins> y <ins>Política de cookies</ins>.<br>
                                    Sed tincidunt velit a accumsan laoreet. Donec vitae est quis ante rhoncus cursus non id ante. Nam fringilla euismod dui, non ullamcorper nunc.<br>
                                    Numllan id vestibulum orci, porttitor lectus. Maecenas scelerisque dignissim erat et faucibus. malesuada non massa ac lobortis.</p>
                                <div style="margin-top: 3%" class="col-xs-12 text-center text-muted">VERITUS© 2014</div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </footer>
        </div>
        <!--Jquery 3.1.1-->
        <script src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <?php
        switch ($this->router->method) {
            case 'index':
                ?>
                <script src="<?php echo base_url(); ?>assets/js/autocomplete.min.js"></script>
                <script src="<?php echo base_url(); ?>assets/js/sweetalert2.min.js"></script>
                <script src="<?php echo base_url(); ?>assets/js/mensajes.js"></script>
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datatables.min.js"></script>
                <script src="<?php echo base_url(); ?>assets/js/inicio.js"></script>
                <?php
                break;

            default:
                break;
        }
        ?>
    </body>
</html>