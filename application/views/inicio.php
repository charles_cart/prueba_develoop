<div class="row">
    <div class="col-xs-12">
        <ol class="breadcrumb"> <span class="hidden-xs">Estas en:</span>
            <li><a href="#"><b>Inicio</b></a></li>
            <li><a href="#"><b>Empresas</b></a></li>
            <li class="active"><b>Registro de Empresas</b></li>
        </ol>
    </div>
    <div class="col-xs-12">
        <div style="min-height: 10px;" class="degradado"></div>
    </div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-xs-12 col-md-6 hidden-xs"><h3>REGISTRO DE EMPRESAS</h3></div>
                <div class="col-xs-12 col-md-6 visible-xs"><h4>REGISTRO DE EMPRESAS</h4></div>
                <div style="min-height: 62px" class="col-xs-12 col-md-6 hidden-xs">
                    <button style="margin-top: 2%" type="button" class="hidden btn btn-primary btn-lg cuadro pull-right btn-save">Guardar cambios</button>
                    <input class="btn btn-primary btn-lg cuadro pull-right btn-save" style="margin-top: 2%" type="submit" value="Guardar cambios" form="form-pyme">
                </div>
                <div class="col-xs-12"><hr></div>
                <div class="col-xs-12">
                    <form id="form-pyme" action="<?php echo site_url('inicio/save_pyme'); ?>" method="POST">
                        <div class="col-xs-12 col-md-12"><h5 class="text-left text-muted">INFORMACI&Oacute;N B&Aacute;SICA</h5></div>
                        <div style="margin-bottom: 20px" class="col-xs-12 col-md-7 bg-info">
                            <div class="form-group col-xs-12 col-md-6">
                                <label for="name_emp" class="control-label">Nombre Comercial Empresa</label>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span></span>
                                    <input name="name_emp" value="<?php echo set_value('name_emp'); ?>" id="name_emp" type="text" class="form-control" placeholder="Nombre de la empresa" aria-describedby="basic-addon1" autocomplete="off">
                                </div>
                                <div style="width: 100%"><?php echo form_error('name_emp', '<div style="font-size: 10px" class="text-danger">', '</div>'); ?></div>
                            </div>
                            <div class="form-group col-xs-12 col-md-6" style="min-height: 59px">
                                <label for="select_type" class="control-label">Tipolog&iacute;a de la empresa</label>
                                <select class="form-control" id="select_type" name="type_emp" value="">
                                    <option class="text-muted" value="">Seleccione</option>
                                    <?php
                                    foreach ($types as $key => $v) {
                                        echo "<option value='" . $v['id'] . "' " . set_select('type_emp', $v['id']) . ">" . $v['name'] . " (" . $v['siglas'] . ")</option>";
                                    }
                                    ?>
                                </select>
                                <div style="width: 100%"><?php echo form_error('type_emp', '<div style="font-size: 10px" class="text-danger">', '</div>'); ?></div>
                            </div>
                            <div class="col-xs-12"></div>

                            <div class="form-group col-xs-12 col-md-4">
                                <label for="pais" class="control-label">Pa&iacute;s</label>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon2"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span></span>
                                    <input value="<?php echo set_value('country_str'); ?>" name="country_str" type="text" class="form-control countries" placeholder="Nombre del pa&iacute;s" aria-describedby="basic-addon2" autocomplete="off">
                                </div>
                                <div style="width: 100%"><?php echo form_error('country', '<div style="font-size: 10px" class="text-danger">', '</div>'); ?></div>
                            </div>

                            <input name="country" value="<?php echo set_value('country'); ?>" class="hidden" type="password" id="select_country">
                            <!--<input name="id" class="hidden" type="password" id="emp_id">-->
                            <div class="form-group col-xs-12 col-md-4">
                                <label for="estado" class="control-label">Estado</label>
                                <input name="estado" value="<?php echo set_value('estado'); ?>" type="text" class="form-control" placeholder="Escribir una provincia" autocomplete="off">
                                <div style="width: 100%"><?php echo form_error('estado', '<div style="font-size: 10px" class="text-danger">', '</div>'); ?></div>
                            </div>

                            <div class="form-group col-xs-12 col-md-4">
                                <label for="city" class="control-label">Ciudad</label>
                                <input name="city" value="<?php echo set_value('city'); ?>" type="text" class="form-control" placeholder="Escribir una ciudad" autocomplete="off">
                                <div style="width: 100%"><?php echo form_error('city', '<div style="font-size: 10px" class="text-danger">', '</div>'); ?></div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-5 pull-right">
                            <div style="padding: 5px 8px 7px 8px !important;" class="col-xs-12 bg-info pull-right">
                                <label for="description" class="control-label">Descripci&oacute;n de la empresa</label>
                                <textarea name="des_emp" class="form-control noresize" rows="13" placeholder="Describir empresa"><?php echo set_value('des_emp'); ?></textarea>
                            </div>
                            <div style="width: 100%"><?php echo form_error('des_emp', '<div style="font-size: 10px" class="text-danger">', '</div>'); ?></div>
                        </div>

                        <div class="col-xs-12 col-md-7 text-muted text-left"><h5>INFORMACI&Oacute;N REGISTRO</h5></div>
                        <div class="col-xs-12 col-md-7 bg-info">
                            <div class="form-group col-xs-12 col-md-4">
                                <label for="email-user" class="control-label">Email/Usuario</label>
                                <input name="user" value="<?php echo set_value('user'); ?>" type="text" class="form-control" placeholder="Escribir email o usuario" autocomplete="off">
                                <div style="width: 100%"><?php echo form_error('user', '<div style="font-size: 10px" class="text-danger">', '</div>'); ?></div>
                            </div>

                            <div class="form-group col-xs-12 col-md-4">
                                <label for="pass" class="control-label">Password</label>
                                <input name="pass" value="<?php echo set_value('pass'); ?>" type="password" class="form-control" placeholder="Escribir Password" autocomplete="off">
                                <div style="width: 100%"><?php echo form_error('pass', '<div style="font-size: 10px" class="text-danger">', '</div>'); ?></div>
                            </div>
                            <div class="form-group col-xs-12 col-md-4">
                                <label for="re-pass" class="control-label">Repetir Password</label>
                                <input name="pass_re" value="<?php echo set_value('pass_re'); ?>" type="password" class="form-control" placeholder="Repetir Password" autocomplete="off"> 
                                <div style="width: 100%"><?php echo form_error('pass_re', '<div style="font-size: 10px" class="text-danger">', '</div>'); ?></div>
                            </div>
                        </div>
                        <!--<div class="col-xs-12"></div>-->
                        <div class="col-xs-12 visible-xs-block">
                            <div class="col-xs-12 text-center">
                                <button style="margin-top: 4%" type="button" class="btn btn-primary btn-lg cuadro pull-right btn-save hidden">Guardar cambios</button>
                                <input class="btn btn-primary btn-lg cuadro btn-save" style="margin-top: 4%" type="submit" value="Guardar cambios" form="form-pyme">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12"><div style="min-height: 10px;" class="degradado"></div></div>
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-xs-12 col-md-6 pull-right">
                    <div class="input-group">
                        <input type="text" class="form-control search_table_emp" placeholder="Buscar por todos los campos" aria-describedby="basic-addon2">
                        <span class="input-group-addon" id="basic-addon2"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></span>
                    </div>
                </div>
                <div class="col-xs-12 text-muted content_table">
                    <table id="table_emp" class="table table-condensed">
                        <thead>
                            <tr>
                                <th>Empresa</th>
                                <th>Tipo</th>
                                <th>Pais</th>
                                <th>Estado</th>
                                <th>Ciudad</th>
                                <th>Usuario</th>
                                <th data-priority="1">Accion</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!--MODALES-->
<div class="modal fade modal_pyme" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title title_pyme">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="form-pyme-edit">
                        <div class="form-group-sm col-xs-12 col-md-6">
                            <label for="name_emp" class="control-label">Nombre Comercial Empresa</label>
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span></span>
                                <input id="name_emp_edit" name="name_emp" type="text" class="form-control" placeholder="Nombre de la empresa" aria-describedby="basic-addon1" autocomplete="off">
                            </div>
                            <div style="width: 100%" id="name_emp_msg"></div>
                        </div>
                        <div class="form-group-sm col-xs-12 col-md-6" style="min-height: 59px">
                            <label for="select_type" class="control-label">Tipolog&iacute;a de la empresa</label>
                            <select id="select_type_edit" class="form-control"  name="type_emp">
                                <option class="text-muted" value="0">Seleccione</option>
                                <?php
                                foreach ($types as $key => $v) {
                                    echo "<option value='" . $v['id'] . "'>" . $v['name'] . " (" . $v['siglas'] . ")</option>";
                                }
                                ?>
                            </select>
                            <div style="width: 100%" id="type_emp_msg"></div>
                        </div>
                        <div class="col-xs-12"></div>
                        <div class="form-group-sm col-xs-12 col-md-4">
                            <label for="pais" class="control-label">Pa&iacute;s</label>
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon4"><span class="glyphicon glyphicon-ok text-success" aria-hidden="true"></span></span>
                                <input  type="text" class="form-control countries_edit" placeholder="Nombre del pa&iacute;s" aria-describedby="basic-addon4" autocomplete="off">
                            </div>
                            <div style="width: 100%" id="country_msg"></div>
                        </div>
                        <input name="country" class="hidden" type="password" id="country_id_edit">
                        <input name="id" class="hidden" type="password" id="id_edit">
                        <div class="form-group-sm col-xs-12 col-md-4">
                            <label for="estado" class="control-label">Estado</label>
                            <input id="estado_edit" name="estado" type="text" class="form-control" placeholder="Escribir una provincia" autocomplete="off">
                            <div style="width: 100%" id="estado_msg"></div>
                        </div>
                        <div class="form-group-sm col-xs-12 col-md-4">
                            <label for="city" class="control-label">Ciudad</label>
                            <input id="city_edit" name="city" type="text" class="form-control" placeholder="Escribir una ciudad" autocomplete="off">
                            <div style="width: 100%" id="city_msg"></div>
                        </div>
                        <div class="col-xs-12 text-muted text-left"><h6>INFORMACI&Oacute;N REGISTRO</h6></div>
                        <div class="form-group-sm col-xs-12 col-md-4">
                            <label for="email-user" class="control-label">Email o Usuario</label>
                            <input id="user_edit" name="user" type="text" class="form-control" placeholder="Escribir email o usuario" autocomplete="off">
                            <div style="width: 100%" id="user_msg"></div>
                        </div>
                        <div class="form-group-sm col-xs-12" style="margin-top: 1%;">
                            <label for="description" class="control-label">Descripci&oacute;n de la empresa</label>
                            <textarea id="des_edit" name="des_emp" class="form-control noresize" rows="5" placeholder="Describir empresa" autocomplete="off"></textarea>
                            <div style="width: 100%" id="des_emp_msg"></div>
                        </div>
                        <div class="form-group-sm col-xs-12" id="id_msg"></div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default cuadro" data-dismiss="modal">Close</button>
<!--                <input class="btn btn-primary cuadro" type="submit" value="Guardar" form="form-pyme-edit">-->
                <button type="button" class="btn btn-primary cuadro btn-save-edit">Guardar</button>
            </div>
        </div>
    </div>
</div>