<?php

class M_inicio extends CI_Model {

    public function get_types() {
        $result = $this->db->select('id, name, siglas')
                ->get('types');
        return ($result->num_rows() > 0) ? $result->result_array() : FALSE;
    }

    public function _get_countries($data) {
        $result = $this->db->select('id data, name value')
                ->like('name', $data['query'])
                ->get('countries');
        return ($result->num_rows() > 0) ? $result->result_array() : FALSE;
    }

    public function saving_pyme($datos) {
        (isset($datos['id'])) ?: $datos['id'] = '';
        $data = [
            'name_emp' => $this->security->xss_clean($datos['name_emp']),
            'type_id' => $datos['type_emp'],
            'country_id' => $datos['country'],
            'estado' => $this->security->xss_clean($datos['estado']),
            'city' => $this->security->xss_clean($datos['city']),
            'user' => $this->security->xss_clean($datos['user']),
            'description' => $this->security->xss_clean($datos['des_emp'])
        ];

        if ($datos['id'] == '') {
            $data['pass'] = md5($datos['pass']);
            $this->db->insert('business', $data);
        } else {
            $this->db->where('id', $datos['id']);
            $this->db->update('business', $data);
        }

        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else if ($datos['id'] != '') {
            return TRUE;
        } else
            return FALSE;
    }

    public function _get_pymes($data) {
        list($order, $columns, $band) = [$data['order'][0], ['name_emp', 'type', 'country', 'estado', 'city', 'user'], TRUE];
        $search = trim($data['search']['value']);
        if ($search == '') {
            $result = $this->db->order_by($columns[$order['column']], $order['dir'])
                    ->get('v_pymes', $data['length'], $data['start']);
        } else {
            $band = FALSE;
            $like = ['name_emp' => $search, 'type' => $search, 'country' => $search, 'estado' => $search, 'city' => $search, 'user' => $search];
            $result = $this->db->limit($data['length'], $data['start'])
                            ->order_by($columns[$order['column']], $order['dir'])
                            ->or_like($like)->get('v_pymes');
        }
        if ($result->num_rows() > 0) {
            if ($band) {
                $count = $this->db->count_all_results('v_pymes');
                return [
                    'success' => TRUE,
                    'draw' => $data['draw'],
                    'recordsTotal' => $count,
                    'recordsFiltered' => $count,
                    'data' => $result->result_array()
                ];
            } else {
                $filtered = $this->db->order_by($columns[$order['column']], $order['dir'])->or_like($like)->get('v_pymes');
                return [
                    'success' => TRUE,
                    'draw' => $data['draw'],
                    'recordsTotal' => $this->db->count_all_results('v_pymes'),
                    'recordsFiltered' => count($filtered->result_array()),
                    'data' => $result->result_array()
                ];
            }
        } else
        if ($band) {
            return [
                'success' => FALSE,
                'draw' => $data['draw'],
                'recordsTotal' => 0,
                'recordsFiltered' => 0,
                'data' => []
            ];
        } else {
            return [
                'success' => FALSE,
                'draw' => $data['draw'],
                'recordsTotal' => $this->db->count_all_results('v_pymes'),
                'recordsFiltered' => 0,
                'data' => []
            ];
        }
    }

    public function _drop_pyme($data) {
        $this->db->delete('business', ['id' => $data['id']]);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

}
