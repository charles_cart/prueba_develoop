<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * Autor: Charles Rodriguez
 */

$config = [
    'inicio/save_pyme' => [
        ['field' => 'name_emp', 'label' => 'Nombre de la empresa',
            'rules' => 'trim|required|max_length[250]|alpha_numeric_spaces|is_unique[v_pymes.name_emp]',
            'errors' => ['required' => 'El %s es requerido.',
                'max_length' => 'Maximo 250 caracteres.',
                'alpha_numeric_spaces' => 'Solo se permiten valores de Aa-Zz y 0-9',
                'is_unique' => 'El nombre de la empresa ya existe.']
        ],
        ['field' => 'type_emp', 'label' => 'Tipo de empresa', 'rules' => 'trim|required|numeric',
            'errors' => ['required' => 'El %s es requerido.',
                'numeric' => 'La opcion seleccionada es invalida']],
        ['field' => 'country', 'label' => 'Pais', 'rules' => 'trim|required|numeric',
            'errors' => ['required' => 'El %s es requerido.',
                'numeric' => 'El pais elegido es invalido']],
        ['field' => 'estado', 'label' => 'estado', 'rules' => 'trim|required|alpha_numeric_spaces|max_length[100]',
            'errors' => ['required' => 'El %s es requerido.',
                'alpha_numeric_spaces' => 'Solo se permiten valores de Aa-Zz y 0-9',
                'max_length' => 'Maximo 100 caracteres']],
        ['field' => 'city', 'label' => 'ciudad', 'rules' => 'trim|required|alpha_numeric_spaces|max_length[100]',
            'errors' => ['required' => 'El %s es requerido.',
                'alpha_numeric_spaces' => 'Solo se permiten valores de Aa-Zz y 0-9',
                'max_length' => 'Maximo 100 caracteres']],
        ['field' => 'des_emp', 'label' => 'Descripcion', 'rules' => 'trim|required|max_length[1000]',
            'errors' => ['required' => 'La %s es requerida',
                'max_length' => 'Maximo 1000 caracteres']],
        ['field' => 'user', 'label' => 'Email/Usuario',
            'rules' => 'trim|required|min_length[5]|max_length[255]|alpha_dash|is_unique[v_pymes.user]',
            'errors' => ['required' => 'El %s es requerido',
                'min_length' => 'El %s debe ser mayor a %d',
                'max_length' => 'Maximo 255 caracteres',
                'alpha_dash' => 'Solo se permiten valores de Aa-Zz | 0-9 | -_',
                'is_unique' => 'El %s ya existe']],
        ['field' => 'pass', 'label' => 'Contraseña', 'rules' => 'trim|required|min_length[5]|max_length[255]',
            'errors' => ['required' => 'La %s es requerida',
                'min_length' => 'La %s debe ser mayor a %d',
                'max_length' => 'Maximo 255 caracteres']],
        ['field' => 'pass_re', 'label' => 'Contraseña', 'rules' => 'trim|required|min_length[5]|max_length[255]|matches[pass]',
            'errors' => ['required' => 'La %s es requerida',
                'min_length' => 'La %s debe ser mayor a %d',
                'max_length' => 'Maximo 255 caracteres',
                'matches' => 'La %s no coincide con la anterior']]
    ],
    'inicio/edit' => [
        ['field' => 'name_emp', 'label' => 'Nombre de la empresa',
            'rules' => 'trim|required|max_length[250]|alpha_numeric_spaces',
            'errors' => ['required' => 'El %s es requerido.',
                'max_length' => 'Maximo 250 caracteres.',
                'alpha_numeric_spaces' => 'Solo se permiten valores de Aa-Zz y 0-9',
                'is_unique' => 'El nombre de la empresa ya existe.']
        ],
        ['field' => 'type_emp', 'label' => 'Tipo de empresa', 'rules' => 'trim|required|numeric',
            'errors' => ['required' => 'El %s es requerido.',
                'numeric' => 'La opcion seleccionada es invalida']],
        ['field' => 'country', 'label' => 'Pais', 'rules' => 'trim|required|numeric',
            'errors' => ['required' => 'El %s es requerido.',
                'numeric' => 'El pais elegido es invalido']],
        ['field' => 'id', 'label' => 'id', 'rules' => 'trim|required|numeric',
            'errors' => ['required' => 'El %s es requerido.',
                'numeric' => '%s es invalido']],
        ['field' => 'estado', 'label' => 'estado', 'rules' => 'trim|required|alpha_numeric_spaces|max_length[100]',
            'errors' => ['required' => 'El %s es requerido.',
                'alpha_numeric_spaces' => 'Solo se permiten valores de Aa-Zz y 0-9',
                'max_length' => 'Maximo 100 caracteres']],
        ['field' => 'city', 'label' => 'ciudad', 'rules' => 'trim|required|alpha_numeric_spaces|max_length[100]',
            'errors' => ['required' => 'El %s es requerido.',
                'alpha_numeric_spaces' => 'Solo se permiten valores de Aa-Zz y 0-9',
                'max_length' => 'Maximo 100 caracteres']],
        ['field' => 'des_emp', 'label' => 'Descripcion', 'rules' => 'trim|required|max_length[10]',
            'errors' => ['required' => 'La %s es requerida',
                'max_length' => 'Maximo 1000 caracteres']],
        ['field' => 'user', 'label' => 'Email/Usuario',
            'rules' => 'trim|required|min_length[5]|max_length[255]|alpha_dash',
            'errors' => ['required' => 'El %s es requerido',
                'min_length' => 'El %s debe ser mayor a %d',
                'max_length' => 'Maximo 255 caracteres',
                'alpha_dash' => 'Solo se permiten valores de Aa-Zz | 0-9 | -_',
                'is_unique' => 'El %s ya existe']]
    ]
];
