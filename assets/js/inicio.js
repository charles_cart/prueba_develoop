/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * Autor: Charles Rodriguez
 */

function Inicio() {
    var self = this;

    this.constructor = function () {
        this.componentes_init();
    }

    this.componentes_init = function () {
        if (welcome)
            msg(welcome, 5000);

        // autocomplete buscador de countries
        $('.countries').devbridgeAutocomplete({
            serviceUrl: base_url + 'index.php/inicio/get_countries',
            type: 'POST',
            minChars: 1,
            onSelect: function (s) {
                $('#select_country').val(s.data);
                $('#basic-addon2').html('<span class="glyphicon glyphicon-ok text-success" aria-hidden="true"></span>');
            },
            onInvalidateSelection: function (s) {
                $('#select_country').val('');
                $('#basic-addon2').html('<span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>');

            }
        });

        // autocomplete buscador de countries modal
        $('.countries_edit').devbridgeAutocomplete({
            serviceUrl: base_url + 'index.php/inicio/get_countries',
            type: 'POST',
            minChars: 1,
            onSelect: function (s) {
                $('#country_id_edit').val(s.data);
                $('#basic-addon4').html('<span class="glyphicon glyphicon-ok text-success" aria-hidden="true"></span>');
            },
            onInvalidateSelection: function (s) {
                $('#country_id_edit').val('');
                $('#basic-addon4').html('<span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>');

            }
        });

        //boton guardar empresa
//        $('.btn-saves').on('click', function (e) {
//            var form = $('#form-pyme').serializeArray();
//            console.log(form);
//            var empty = false;
//            $.each(form, function (i, field) {
//                if (field.name != 'country' && field.name != 'type_emp' && field.value == '') {
//                    empty = true;
//                    msg(50);
//                }
//            });
//            if (form[6].value != form[7].value) {
//                empty = true;
//                msg(53);
//            }
//            if (form[5].value.length < 5) {
//                empty = true;
//                msg(52);
//            }
//            if (empty)
//                return 0;
//            $.ajax({
//                url: base_url + 'index.php/inicio/save_pyme',
//                type: 'POST',
//                dataType: 'json',
//                data: form
//            })
//                    .done(function (data) {
//                        if (data.data instanceof Object) {
//                        } else {
//                            msg(data.data);
//                            if (data.success == true) {
//                                $('#form-pyme')[0].reset();
//                                $('#basic-addon2').html('<span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>');
//                                table_emp.ajax.reload();
//                            }
//                        }
//                    })
//                    .fail(function () {
//                        msg(67);
//                    })
//
//        });

        // DATA TABLE DE CUENTAS
        var table_emp = $('#table_emp').DataTable({
            "processing": true,
            "serverSide": true,
            "sPaginationType": "full_numbers",
            'autoWidth': false,
            'select': true,
            'language': datatables_spanish,
            'bLengthChange': false,
            'sDom': 'rtip',
            'responsive': true,
            'ajax': {
                'url': base_url + 'index.php/inicio/get_pymes',
                'type': 'POST'
            },
            columnDefs: [{targets: [], visible: false}, {'bSortable': false, 'aTargets': [6]}],
            'columns': [
                {'data': 'name_emp', 'defaultContent': 'No Aplica'},
                {'data': 'type', className: ''},
                {'data': 'country'/*, 'width': '50px'*/},
                {'data': 'estado', 'defaultContent': 'No Aplica'},
                {'data': 'city', 'defaultContent': 'No Aplica'},
                {'data': 'user', 'defaultContent': 'No Aplica'},
                {"data": null, "width": "6%", className: "text-center", "defaultContent": "<div class=''><div class='btn-group'><button type='button' class='btn btn-primary btn-xs dropdown-toggle' data-toggle='dropdown' aria-expanded='false'> Actions <span class='caret'></span></button><ul style='font-size: 12px;' class='dropdown-menu pull-right' role='menu'><li><a href='#' class=' btn_edit negro'><span style='color: #0000FF; margin: 0 5px 0 -7px;' class='glyphicon glyphicon-pencil'></span> Editar</a></li><li><a href='#' class=' btn_drop negro'><span style='color: #990000; margin: 0 5px 0 -7px;' class='glyphicon glyphicon-remove'></span> Eliminar</a></li></ul></div></div>"}, ]
        });
        table_emp
                .on('select', function (e, dt, type, indexes) {
                    var rowData = table_emp.rows(indexes).data().toArray();
                    select_pyme = rowData[0];
//                    console.log(select_pyme);
                });

        // input search de datatable
        $('.search_table_emp').on('keyup', function () {
            table_emp.search(this.value).draw();
        });

        //    BOTON MODIFICAR PYME
        $("#table_emp").on('click', ".btn_edit", function (e) {
            self.load_modal_edit();
            $('.modal_pyme').modal({
                keyboard: false,
                backdrop: 'static'
            });
        });

        //    BOTON DELETE PYME
        $("#table_emp").on('click', ".btn_drop", function (e) {
            swal({title: 'Desea eliminar esta empresa?',
                showCancelButton: true, type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Eliminar', }).then(
                    function (result) {
                        $.ajax({
                            url: base_url + 'index.php/inicio/drop_pyme',
                            type: 'POST',
                            dataType: 'json',
                            data: {id: select_pyme.id}
                        })
                                .done(function (data) {
                                    if (data.data instanceof Object) {
                                    } else {
                                        msg(data.data);
                                        if (data.success == true)
                                            table_emp.ajax.reload();
                                    }
                                })
                                .fail(function () {
                                    msg(67);
                                })
                    }, function (dismiss) {}
            );
        });

        //boton guardar empresa
        $('.btn-save-edit').on('click', function (e) {
            var form = $('#form-pyme-edit').serializeArray();
            var valid = true;
            console.log(form);
            $.each(form, function (i, field) {
                if (field.value == '') {
                    msg(50);
                    valid = false;
                    return 0;
                }
                if (field.name == 'user') {
                    if (field.value.length < 5) {
                        msg(52);
                        valid = false;
                        return 0;
                    }
                }
            });
            $.ajax({
                url: base_url + 'index.php/inicio/edit',
                type: 'POST',
                dataType: 'json',
                data: form
            })
                    .done(function (data) {
                        if (data.success == true) {
                            table_emp.ajax.reload();
                            msg(data.data);
                            $.each(data.clear, function (i, value) {
                                $('#' + i).html(value);
                            });
                        } else {
                            $.each(data.data, function (i, value) {
                                $('#' + i).html(value);
                            });
                        }
                    })
                    .fail(function () {
                        msg(67);
                    })
        });

        // validacion de form-pyme a nivel de JS
        $('#form-pyme').on('submit', function (e) {
            e.preventDefault();
            var form = $(this);
            var valid = true;
//            console.log(form.serializeArray());
            $.each(form.serializeArray(), function (i, field) {
                if (field.value == '') {
                    msg(50);
                    valid = false;
                    return 0;
                }
                if (field.name == 'user' || field.name == 'pass') {
                    if (field.value.length < 5) {
                        if (field.name == 'user')
                            msg(52);
                        else
                            msg(54);
                        valid = false;
                        return 0;
                    }
                }
            });
            if (form.serializeArray()[8]['value'] != form.serializeArray()[9]['value']) {
                msg(53);
                valid = false;
                return 0;
            }
            if (valid)
                this.submit();
        });

        $('#table_emp_info').addClass('hidden-xs hidden-sm');

    }

    this.load_modal_edit = function () {
        $('#name_emp_edit').prop('value', select_pyme.name_emp);
        $('#select_type_edit').prop('value', select_pyme.type_id);
        $('.countries_edit').prop('value', select_pyme.country);
        $('#country_id_edit').prop('value', select_pyme.country_id);
        $('#id_edit').prop('value', select_pyme.id);
        $('#estado_edit').prop('value', select_pyme.estado);
        $('#city_edit').prop('value', select_pyme.city);
        $('#user_edit').prop('value', select_pyme.user);
        $('#des_edit').prop('value', select_pyme.description);
        $('.title_pyme').html('Editando a ' + select_pyme.name_emp);
    }
}

$(function () {
    var inicio = new Inicio();
    inicio.constructor();
});
