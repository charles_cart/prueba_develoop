function msg(num) {
    var msg = Array();
//    ERROR
    msg[-1] = Array('Operación Cancelada', 'No pudo realizarse la operación!');
    msg[-2] = Array('Operación Cancelada', 'La contratacion no ha sido eliminada!');
    msg[-3] = Array('En Desarrollo', '<div class="text-justify">Eligio un cliente B2B y el editar de este tipo de usuario esta en construccion, disculpe!<br><br><b>Para el dia viernes 01-07-2016 a las 3pm esta funcionalidad se encontrara operativa.</b></div><br><b>El tipo de cliente B2C si se puede editar.</b>');
    msg[-4] = Array('Operación Cancelada', 'La tabla no pudo ser cargada con los datos porvenientes de la base de datos!');
    msg[-5] = Array('Monto Invalido', 'Ha introducido un valor el cual consideramos que no es numerico, por favor introduce un monto valido!<br><br><div class="text-justify">-. Verifica que el monto no sea negativo<br>-. Verifica que los decimales los especifiques con punto "."</div>');
    msg[-6] = Array('Operación Cancelada', 'Las fechas se solapan!');
//    SUCCESS
    msg[1] = Array('Success', 'Operación realizada con éxito');
    msg[2] = Array('Eliminado!', 'Registro Borrado Correctamente');
    msg[3] = 'Registro Almacenado Correctamente';
    msg[4] = Array('Success', '<b>Operación realizada con éxito.</b><br><div class="text-justify"><br>Se le ha enviado la información necesaria para iniciar sesion al correo electronico del cliente registrado.</div>');
    msg[5] = Array('Success', '<b>Email enviado con éxito.</b>');

    /*ADVERTENCIAS Y WARNING*/
    msg[50] = Array('Campos vacios', 'Debe rellenar los campos obligatorios');
    msg[51] = Array('Campos vacios', 'Seleccione correctamente un tipo de empresa o país');
    msg[52] = Array('Longitud muy corta', 'La longitud del email o usuario es muy corta');
    msg[53] = Array('Contraseña invalida', 'La contraseña no coincide, por favor verificar');
    msg[54] = Array('Longitud muy corta', 'La longituda de tu contraseña es muy corta');
    msg[55] = 'No se pudo establecer conexión con el servidor LDAP';
    msg[56] = 'Expiró la sesión';
    msg[57] = 'Disculpe Ud. no pertenece a PDVSA Costa Afuera';
    msg[58] = 'Problemas para registrar el usuario';
    msg[59] = ['Error al asignar privilegios','Problemas para asignar los permisos al usuario, debido a esto se negara el acceso al sistema!'];
    msg[60] = 'Problemas para cargar los modulos';
    msg[61] = 'Verifique los datos de la información!';
    msg[62] = 'No hay datos de producción almacenados';
    msg[63] = 'No hay registros de grupos almacenados';
    msg[64] = 'Problema al Guardar los Datos';
    msg[65] = 'El registro no puede ser eliminado, hay usuarios vinculados';
    msg[66] = 'El registro no puede ser eliminado, el grupo está vinculado';
    msg[67] = Array('Error', 'Error al realizar la operación');
    msg[68] = Array('Error', 'No hay datos registrados en la base de datos');
    msg[69] = Array('Error', 'El email no pudo ser enviado, por lo tanto el usuario no pudo ser activado!');
    msg[70] = 'Problema al Guardar los Datos, ya hay un sistema con ese nombre';
    msg[71] = 'Operación fallida, el usuario ya se encuentra registrado';
    msg[72] = 'No posee ninguna funcionalidad asignada';
    msg[73] = 'Problema al Guardar los Datos, ya hay un grupo con ese nombre';
    msg[74] = 'No hay datos de producción almacenados en la fecha seleccionada';
    msg[75] = 'Problema para cargar el menú de opciones';
    msg[76] = 'Datos no encontrados';
    msg[77] = 'Página no encontrada';
    msg[78] = 'Problema para guardar el Grupo';
    msg[79] = Array('Error al eliminar', 'Problemas al eliminar el registro.');
    msg[80] = Array('Error', 'Limite maximo alcanzado!');
    msg[81] = Array('Permiso Denegado', 'No tiene Permisos suficientes para acceder a esta funcionalidad del sistema. Pongase en contacto con los desarrolladores!<br>sistemaricana1@gmail.com');
    msg[82] = Array('Error', 'Sin destinos de traslado!');
    msg[83] = Array('Error', 'Sin hoteles para este filtro!');
    msg[84] = Array('Error', 'Sin descripcion!');
    msg[85] = Array('Error', 'Sin destinos disponibles para alojamiento!');
    msg[86] = Array('Error', 'Sin destinos disponibles para opcionales!');
    msg[87] = Array('Error', 'Sin planes disponibles para este hotel!');
    msg[88] = Array('Error', 'Sin contrataciones disponibles para este tipo de contratación!');
    msg[89] = Array('Error', 'Ha Excedido el rango de descuento permitido para esta contratación');
    msg[90] = Array('Error', 'Solo valores numericos');
    msg[91] = Array('Advertencia', 'No le puedes aplicar un descuento debido a que has seleccionado un MarkUp No Comisionable!');
    msg[92] = Array('Error', 'Si vas aplicar un descuento, el valor minimo aceptado es 1%, si no piensas aplicar un descuento a este cliente deberias deseleccionar el Check "Aplicar Desc."');
    msg[93] = Array('Ya Posee una contratación', 'Este cliente ya posee una contratación de tipo <b>Global</b>, no se permite tener dos contrataciones de tipo <b>Global</b> para un solo cliente y para agregar un contratación especifica por <b>Alojamiento</b>, <b>Opcionales</b>, <b>Traslados</b>, y <b>Renta Auto</b> debe primeramente eliminar la contratación <b>Global</b> que ya posee el cliente.');
    msg[94] = Array('Error', 'Por favor seleccione un cliente primeramente y luego ejecute la funcionalidad de eliminar.');
    msg[95] = Array('Error', 'La dirección de correo electronica del cliente es invalida.');
    msg[96] = Array('Error', 'La dirección de correo electronica para este contacto es invalida.');
    msg[97] = Array('Advertencia!', 'Para agregar una contratacion primero debes seleccionar un cliente de la tabla usuarios.');
    msg[98] = Array('Advertencia!', 'Seleccione una forma de pago!.');
    msg[99] = Array('Advertencia!', 'Tu contraseña para validar pagos es incorrecta, al tercer intento erroneo tu usuario sera boqueado preventivamente!');
    msg[100] = Array('Advertencia!', 'Si vas a validar el pago es necesario que introduzcas tu contraseña de validacion de pagos!');
    msg[101] = Array('Error', 'al parecer tiene  un tipo de habitaciones repetidas');
    msg[102] = Array('Error', 'No puede cambiar el hotel, por que tiene habitaciones asociadas a el. Si quiere actualizar elimine las habitaciones asociadas a el');
    msg[103] = Array('Error', 'no puede actualizar su cantidad global por un valor menor, de la sumatoria de sus habitaciones');
    msg[104] = Array('Error', 'Este correo ya se encuentra registrado');
    msg[105] = Array('Error', 'La información que desea eliminar ya esta siendo utilizada');
    msg[106] = Array('Error', 'Debe selecionar alguna frecuencia de dias');
    msg[107] = Array('Error', 'la fecha de inicio no puede ser mayor a la fecha final');
    msg[108] = Array('Error', 'la fecha inicio no esta dentro del rango de la fecha del allotment');
    msg[109] = Array('Error', 'la fecha final no esta dentro del rango de la fecha del allotment');
    msg[110] = Array('Advertencia!', 'No se pudo enviar el email del B2B, por lo tanto no se guardo ningun dato, vuelva a intentarlo!');
    msg[111] = Array('Advertencia!', 'No se pudo enviar el email a los empleados del B2B, por lo tanto no se guardo ningun dato, vuelva a intentarlo!');
    msg[112] = Array('Advertencia!', 'Solo puede agregarle contrataciones a clientes B2C y B2B. El cliente seleccionado es un empleado de un B2B');
    msg[113] = ['Advertencia!', 'el distino que desea intentar agregar ya existe'];
    msg[114] = ['Advertencia!', 'la cadena que desea intentar agregar ya existe'];
    msg[115] = ['Advertencia!', 'la habitacion que desea intentar agregar ya existe'];
    msg[116] = ['Advertencia!', 'este hotel ya se encuentra registrado'];
    msg[117] = ['Advertencia!', 'la clasificacion que intenta agregar ya existe'];


    if (num < 0)
        return swal({title: msg[num][0], type: 'error', text: msg[num][1], html: msg[num][1]});
    if (num < 50)
        return swal({title: msg[num][0], type: 'success', text: msg[num][1], html: msg[num][1]});
    else
        return swal({title: msg[num][0], type: 'warning', text: msg[num][1], html: msg[num][1]});
}

function out() {
    $("#alerta").fadeOut();
}
